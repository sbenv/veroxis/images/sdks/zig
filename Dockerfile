FROM registry.gitlab.com/sbenv/veroxis/images/zig:0.13.0 as zig
FROM registry.gitlab.com/sbenv/veroxis/images/macosx-sdks:13.0 as macosx-sdk

# ---

FROM registry.gitlab.com/sbenv/veroxis/images/sdks/alpine:3.20.2

COPY --from=zig "/usr/bin/zig" "/usr/bin/zig"
COPY --from=zig "/usr/lib/zig" "/usr/lib/zig"
COPY --from=zig "/usr/share/licenses/zig/LICENSE" "/usr/share/licenses/zig/LICENSE"
RUN chmod -R g+w,o+w "/usr/lib/zig"

COPY --from=macosx-sdk "/opt/MacOSX.sdk" "/opt/MacOSX.sdk"
ENV SDKROOT=/opt/MacOSX.sdk
